'use strict';

/**
* majalahsunnahApp le
*
* Description
*/
angular.module('majalahsunnahApp')
  .controller('SectionCtrl', ['Sections', '$scope',
  	function(Sections, $scope){
  		Sections.query(
  			function(results){
  				$scope.sections = results;
  			},
  			function(err){
  				return err;
  			}
  			)  	
  }]);