'use strict';

/**
* majalahsunnahApp Module
*
* Description
*/
angular.module('majalahsunnahApp')
  .controller('ArticlesCtrl', ['$scope', 'articles',
  	function($scope, articles){
  	  $scope.articles = articles;
  }]);