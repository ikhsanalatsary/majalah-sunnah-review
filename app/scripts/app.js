'use strict';

angular
  .module('majalahsunnahApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute',
    'Services'
  ])
  .config(function ($routeProvider) {
    var articlesPromise = function(ArticlesLoader) {
                            return ArticlesLoader();
                          };
    $routeProvider
      .when('/', {
        templateUrl: 'views/articles.html',
        controller: 'ArticlesCtrl',
        resolve: {
          articles: articlesPromise
        }
      })     
      .otherwise({
        redirectTo: '/'
      });
  });
