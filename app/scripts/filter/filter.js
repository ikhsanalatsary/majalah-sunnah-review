'use strict';

/**
* Filter Module
*
* Description
*/
angular.module('Filter', [])
  .filter('replace', [function() {
    return function(result){
      if(result){
        var r = result.replace(/\\u003C/g, '<')
                                    .replace(/\\u003E/g, '>')
                                    .replace(/\n/g, '')
                                    .replace(/\\u0026/g,'&');
      return r;
      }
    }  
}]);

