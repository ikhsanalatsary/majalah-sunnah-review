'use strict'

/**
* majalah Module
*
* Description
*/
angular.module('Services', [])
  .factory('Sections', ['$resource',
  	function($resource){
  	  return $resource('http://api.situssunnah.com/api/sections.json'); 	
    }])
  .factory('Articles', ['$resource',
  	function($resource){
  	  return $resource('http://api.situssunnah.com/api/articles/:id.:format',
  	  					{id: '@id', format: 'json'});
  }])
  .factory('ArticlesLoader',['Articles', '$q',
  	function(Articles, $q){
  	  return function(){
  	  	var delay = $q.defer();
  	  	Articles.query(function(articles){
  	  	  delay.resolve(articles);
  	  	}, function(){
  	  		delay.reject('Unable to fetch articles');
  	  	});
  	  	return delay.promise
  	  }	;
  	}]);