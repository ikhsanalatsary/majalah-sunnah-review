'use strict';

describe('filter', function() {
	beforeEach(module('Filter'));

	describe('replace', function() {
		it('should convert unicode to tag html',
			inject(function(replaceFilter){
				expect(replaceFilter('\\u003C')).toBe('<');
				expect(replaceFilter('\\u003E')).toBe('>');
				expect(replaceFilter('\n')).toBe('');
				expect(replaceFilter('\\u0026')).toBe('&');
		}));
	});				
});